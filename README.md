<!-- This is a comment in md (Markdown) format, it will not be visible to the end user -->

<!-- Update the below line with your Pre-Built name -->
# Slack Notification

<!-- Leave TOC intact unless you've added or removed headers -->
## Table of Contents

* [Overview](#overview)
  * [Operations Manager and JSON-Form](#automation-catalog-and-json-form)
* [Installation Prerequisites](#installation-prerequisites)
* [Requirements](#requirements)
* [Features](#features)
* [How to Install](#how-to-install)
* [How to Run](#how-to-run)
  * [Input Variables](#input-variables)
* [Additional Information](#additional-information)

## Overview
This Pre-Built integrates with the [Slack Open Source Adapter](https://gitlab.com/itentialopensource/adapters/notification-messaging/adapter-slack) to send a notification within Slack.

## Operations Manager and JSON-Form

This workflow has an [Operations Manager (AC) Item](./bundles/ac_agenda_jobs/Slack%20Notification.json) that calls a workflow. The AC Item uses a JSON-Form to specify common fields populated when an issue is created. The workflow the AC item calls queries data from the formData job variable.

<table>
  <tr>
    <td>
      <img src="./images/slack_notification_ac_form.png" alt="form" width="800px">
    </td>
  </tr>
  <tr>
    <td>
      <img src="./images/slack_notification_canvas.png" alt="form" width="800px">
    </td>
  </tr>
</table>

## Installation Prerequisites

Users must satisfy the following pre-requisites:

- Itential Automation Platform
  - `^2023.1`
- [Slack Open Source Adapter](https://gitlab.com/itentialopensource/adapters/notification-messaging/adapter-slack)
  - `^2.5.2`

## Requirements

This Pre-Built requires the following:

- [Slack Open Source Adapter](https://gitlab.com/itentialopensource/adapters/notification-messaging/adapter-slack) that is configured with a Slack server

## Features

The main benefits and features of the Pre-Built are outlined below.

* Allows a user to send a notification to a specific Slack channel

## How to Install

To install the Pre-Built:

* Verify you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Requirements](#requirements) section in order to install the Pre-Built. 
* The Pre-Built can be installed from within App-Admin_Essential. Simply search for the name of your desired Pre-Built and click the install button.

## How to Run

Use the following to run the Pre-Built:

* Run the Operations Manager Item `Slack Notification` or call [Slack Notification API](./bundles/workflows/Slack%20Notification%20API.json) from your workflow as a child job.

### Input Variables
_Example_

```json
{
  "channelName": "Specific Slack channel in which to send the notification",
  "message": "The notification message to be sent"
}
```

## Additional Information

Please use your Itential Customer Success account if you need support when using this Pre-Built.
