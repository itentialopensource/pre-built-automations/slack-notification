
## 0.0.19 [05-25-2023]

* Merging pre-release/2023.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/slack-notification!11

---

## 0.0.18 [06-24-2022]

* Update README.md

See merge request itentialopensource/pre-built-automations/slack-notification!9

---

## 0.0.17 [01-05-2022]

* Certifications 21.2

See merge request itentialopensource/pre-built-automations/slack-notification!8

---

## 0.0.16 [11-15-2021]

* Update pre-built description

See merge request itentialopensource/pre-built-automations/slack-notification!6

---

## 0.0.16 [11-15-2021]

* patch/dsup 1000

See merge request itentialopensource/pre-built-automations/slack-notification!7

---

## 0.0.15 [07-09-2021]

* Update package.json

See merge request itentialopensource/pre-built-automations/slack-notification!5

---

## 0.0.14 [05-17-2021]

* Update package.json

See merge request itentialopensource/pre-built-automations/slack-notification!1

---

## 0.0.13 [05-06-2021]

* Update package.json

See merge request itentialopensource/pre-built-automations/slack-notification!1

---

## 0.0.12 [05-05-2021]

* Update README.md, images/slack_notification_ac_form.png, images/slack_notification_canvas.png files

See merge request itential/sales-engineer/selabprebuilts/slack-notification!5

---

## 0.0.11 [03-23-2021]

* Update README.md, images/slack_notification_ac_form.png, images/slack_notification_canvas.png files

See merge request itential/sales-engineer/selabprebuilts/slack-notification!5

---

## 0.0.10 [03-01-2021]

* Update README.md, images/slack_notification_ac_form.png, images/slack_notification_canvas.png files

See merge request itential/sales-engineer/selabprebuilts/slack-notification!5

---

## 0.0.9 [03-01-2021]

* patch/2021-03-01T08-31-56

See merge request itential/sales-engineer/selabprebuilts/slack-notification!4

---

## 0.0.8 [02-25-2021]

* Update README.md

See merge request itential/sales-engineer/selabdemos/notification-slack!3

---

## 0.0.7 [02-25-2021]

* Update images/NotificationSlackAutoCatalogForm.png,...

See merge request itential/sales-engineer/selabdemos/notification-slack!2

---

## 0.0.6 [02-25-2021]

* Update images/NotificationSlackAutoCatalogForm.png,...

See merge request itential/sales-engineer/selabdemos/notification-slack!2

---

## 0.0.5 [02-24-2021]

* Bug fixes and performance improvements

See commit 04c95a9

---

## 0.0.4 [02-05-2021]

* Bug fixes and performance improvements

See commit 0757a47

---

## 0.0.3 [02-05-2021]

* Bug fixes and performance improvements

See commit 78d4e45

---

## 0.0.2 [01-29-2021]

* Bug fixes and performance improvements

See commit 5ad2b6d

---\n
